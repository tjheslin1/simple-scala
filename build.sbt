lazy val root = (project in file("."))
  .settings(
    libraryDependencies ++= Seq(
      // format: off
      "org.typelevel" %% "cats-core" % "1.6.0"
    )
  )